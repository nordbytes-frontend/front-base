/**
 * Created by harut55555 on 10/7/16.
 */
module.exports = function(grunt) {

    grunt.initConfig({
        exec: {
            remove_logs: {
                command: 'rm -f *.log',
                stdout: false,
                stderr: false
            },
            list_files: {
                cmd: 'ls -l **'
            },
            list_all_files: 'ls -la',
            echo_grunt_version: {
                cmd: function() { return 'echo ' + this.version; }
            },
            echo_name: {
                cmd: function(firstName, lastName) {
                    var formattedName = [
                        lastName.toUpperCase(),
                        firstName.toUpperCase()
                    ].join(', ');

                    return 'echo ' + formattedName;
                }
            }
        }
    });

    grunt.task.registerTask('build', 'A sample task that logs stuff.', function(arg1, arg2) {
        if (arguments.length === 0) {
            grunt.log.writeln(this.name + ", no args");
        } else {
            grunt.log.writeln(this.name + ", " + arg1 + " " + arg2);
        }
        grunt.task.run("exec", 'list_files');
    });

    grunt.loadNgpmTasks('grunt-exec');

    grunt.registerTask('default', ['exec:list_files']);
};