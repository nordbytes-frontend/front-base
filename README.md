# front-base
React + Redux + Material UI + ES6

* [React](https://facebook.github.io/react/)
* [Redux](http://rackt.org/redux/index.html)
* [Material UI](http://material-ui.com/#/)
* [webpack](https://webpack.github.io/)
* [Babel](https://babeljs.io/)
* [ESLint](http://eslint.org/)


# Usage


## Preparation
```bash
$ git add -A
$ git commit -m "Initial commit with boilerplate"
```

## Package installation
```bash
$ npm install
```

## Use development server
For development server, webpack-dev-server is reasonable. It monitors update files and rebuild them automatically. Since webpack cli command is registerd in `package.json` in this project, just type following command to run webpack-dev-server.

```bash
$ npm start
```

## Build assets
To put compiled files into `static` directory, type the following command.

```bash
$ npm run build
```

