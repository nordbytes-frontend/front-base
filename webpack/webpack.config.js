"use strict";

var webpack = require('webpack');
var path = require('path');
var HtmlWebpackPlugin = require('html-webpack-plugin');
var ExtractTextPlugin = require('extract-text-webpack-plugin');

const HOST = process.env.HOST || "127.0.0.1";
const PORT = process.env.PORT || "8888";
const APP_NAME = process.env.APP_NAME;

module.exports =  {
    context: __dirname,
    entry: {
      jsx: "../_apps/" + APP_NAME + "/src/index.jsx",
      // css: "../_apps/" + APP_NAME + "/src/main.css",
      html: "../_apps/" + APP_NAME + "/src/index.html"
    },

    output: {
      path: "../../static/" + APP_NAME,
      filename: "bundle.js",
    },
    module: {
      preLoaders: [
        //Eslint loader
        {test: /\.jsx?$/, exclude: /node_modules/, loader: "eslint-loader"},
      ],
      loaders: [
        {test: /\.html$/, loader: "file?name=[name].[ext]"},
        {test: /\.css$/, loader: "file?name=[name].[ext]"},
        {test: /\.jsx?$/, exclude: /node_modules/, loaders: ["babel-loader"]},
          {
              test: /\.scss$/,
              loader: ExtractTextPlugin.extract('css!sass')
          }
          // {
          //     test: /\.scss$/,
          //     loaders: ['style', 'css', 'sass']
          // }
      ],
    },
    plugins: [
        new ExtractTextPlugin('../../static/'+APP_NAME+'/style.css', {
            allChunks: true
        })
    ],
    resolve: {
      extensions: ['', '.js', '.jsx']
    },
    eslint: {
      configFile: '../'+APP_NAME+'/.eslintrc'
    },
    devServer: {
        contentBase: "../../static/" + APP_NAME,
        // do not print bundle build stats
        noInfo: true,
        // enable HMR
        hot: true,
        // embed the webpack-dev-server runtime into the bundle
        inline: true,
        // serve index.html in place of 404 responses to allow HTML5 history
        historyApiFallback: true,
        port: PORT,
        host: HOST
    },
};