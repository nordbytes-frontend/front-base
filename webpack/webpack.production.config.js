module.exports =  {
    context: __dirname,
    entry: {
      jsx: process.env.NODE_SRC || "./src/index.jsx",
      css: process.env.NODE_CSS || "./src/main.css",
      html: process.env.NODE_HTML || "./src/index.html"
    },

    output: {
      path: process.env.NODE_DIR,
      // path: __dirname + "/static",
      filename: "bundle.js",
    },
    module: {
      preLoaders: [
        //Eslint loader
        {test: /\.jsx?$/, exclude: /node_modules/, loader: "eslint-loader"},
      ],
      loaders: [
        {test: /\.html$/, loader: "file?name=[name].[ext]"},
        {test: /\.css$/, loader: "file?name=[name].[ext]"},
        {test: /\.jsx?$/, exclude: /node_modules/, loaders: ["babel-loader"]},
      ],
    },
    resolve: {
      extensions: ['', '.js', '.jsx']
    },
    eslint: {
      configFile: './.eslintrc'
    },
};